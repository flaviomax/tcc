import numpy as np
import math
import pickle
from multiprocessing import Pool
import time
import os, argparse
from random import randint
from random import sample
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.metrics import precision_recall_fscore_support
import logging
from ClassifierGenerator import ClassifierGenerator
from OperationsGenerator import OperationsGenerator
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

base = '/home/flavio/unicamp/tcc/resultFeatures3Classes/'

class AnyPermutationSolver:
    def __init__(self, initial_perm=None, perm_size=10, initial_perm_code=None, clf=None):
        self.initial_perm_code = initial_perm_code
        self.perm_size = perm_size
        if initial_perm_code != None:
            self.initial_perm = self.decod_perm(self.initial_perm_code)
        else:
            self.initial_perm = initial_perm
        self.base = base
        self.operations_generator = OperationsGenerator(self.perm_size)
        self.max_rounds = self.perm_size * 3

        if clf != None:
            self.clf = clf
        else:
            self.clf = self.load_classifier()

    def load_classifier(self):
        try:
            clf = pickle.load(open('classifier.pickle', 'r'))
        except IOError:
            logging.error('Could not find classifier.')
            exit()

        return clf
        
    def unrank(self, n, r, current_perm):
        if n > 0:
            current_perm[n-1], current_perm[r % n] = current_perm[r % n], current_perm[n-1]
            self.unrank(n-1, int(r/n), current_perm)
    
    def decod_perm(self, perm_code):
        '''
        @returns: list with permutation
        '''
        n = self.perm_size
        result = list(range(1, n + 1))
        self.unrank(n, perm_code - 1, result)
        return result
    
    def load_permutation_data(self, perm_code):
        result = np.loadtxt(self.base + str(perm_code).zfill(7), delimiter=',')
        return result

    def load_permutation_matrix(self, perm):
        result = {}
        result['feature_matrix'], result['target_perms'] = self.operations_generator.get_feature_matrix(perm)
        result['operations'] = self.operations_generator.reversion_operations + self.operations_generator.transposition_operations

        # TODO:
        # MUDAR ISSO! Ta botando 1 pra todos por enquanto
        # result['distance_improvement'] = np.asarray([1] * len(result['target_perms']))

        # UPDATE:
        # Nao conseguimos essa informacao pra permutacoes grandes

        return result

    
    def classify_possible_permutations(self, current_perm=None):
        if current_perm == None and self.initial_perm != None:
            current_perm = self.initial_perm
        if current_perm == None:
            logging.error('Cant find current_perm')
            raise Exception('NullCode')

        # if current_perm == list(range(1, self.perm_size + 1)):
        #     return current_perm

        perm_data = self.load_permutation_matrix(current_perm)
        # perm_data = self.load_permutation_data(current_perm)

        #  IMPLEMENTAR
        target_perms = perm_data['target_perms']
        operations_codes = perm_data['operations']
        feature_vectors = perm_data['feature_matrix']
        # y = perm_data['distance_improvement'] #we dont know that anymore
        
        improve_prob = self.clf.predict_proba(feature_vectors)
#         print improve_prob
        best_op = int(np.argmax(improve_prob[:, 1]))
        logging.debug('Chose %s as best new permutation, with %2f confidence' % (
                    str(target_perms[best_op]),
                    np.max(improve_prob))
            )

        # if y[best_op] == 1:
        #     logging.debug('It was a good decision! Distance was improved -- not reliable yet')
        # elif y[best_op] == 0:
        #     logging.debug('It was a neutral decision. Distance kept.')
        # else:
        #     logging.debug('''It was a BAD decision :( Distance increased''')

        return target_perms[best_op], self.operations_generator.operations[best_op]
    
    def run(self):
        logging.info('Starting run for permutation %s' %
                    str(self.initial_perm))
        start = time.time()
        rnd = 0

        necessary_operations = []

        if self.initial_perm != None:
            current_perm = list(self.initial_perm)
        if current_perm == list(range(1, self.perm_size + 1)):
            return 0, []

        while rnd < self.max_rounds:
            rnd += 1
            new_perm, operation = self.classify_possible_permutations(current_perm)
            necessary_operations.append(operation)
            if new_perm != list(range(1, self.perm_size + 1)): #if it is not the solution
                current_perm = new_perm
            else:
                elapsed = time.time() - start
                logging.info('Solution found in %d steps and %f seconds' % (rnd, elapsed))
                return rnd, necessary_operations

            if rnd == self.max_rounds:
                elapsed = time.time() - start
                logging.info('''Solution could not be found. %f seconds spent''' % elapsed)
                return -1, -1



if __name__ == '__main__':
    # perm_code, gen_needed = parse_args(sys.argv[1:])
    parser = argparse.ArgumentParser()
    # parser.add_argument('-g', action='store_true', help='generate new classifier')
    parser.add_argument('-p', nargs='?', help='permutation code')
    # parser.print_help()
    args = parser.parse_args()
    try:
        perm_code = int(args.p)
    except:
        parser.print_help()
        exit()

    # gen_needed = args.g

    # if gen_needed:
    #     logging.info('Generating Classifier...')
    #     gen = ClassifierGenerator(base=base)
    #     clf = gen.generate() # here we get the classifier
    #     pickle.dump(clf, open('classifier.pickle', 'w'))
    #     logging.info('Classifier generated successfully.')

    else:
        try:
            clf = pickle.load(open('classifier.pickle', 'r'))
        except IOError:
            logging.error('Could not find classifier.')
            exit()

    logging.info('Starting permutation solver...')

    perm_solver = AnyPermutationSolver(perm_code, perm_size=10)
    perm_solver.run()

    logging.info('Done!')