import numpy as np
import math
import pickle
from multiprocessing import Pool
import time
import sys
from random import randint
from random import sample
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.metrics import precision_recall_fscore_support
import logging
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

base = '/home/flavio/unicamp/tcc/resultFeatures3Classes/'

class ClassifierGenerator:
    def __init__(self, base, num_of_perms=1000):
        self.base = base
        self.numOfFiles = math.factorial(10)
        self.num_of_perms = num_of_perms
    
    def getDataFromPerms(self, numOfPerms):
        data = []

        for i in range(numOfPerms):
            filename = randint(1, self.numOfFiles)
            a = np.loadtxt(self.base + str(filename).zfill(7), delimiter=',')
            data.append(a)

        data = [item for sublist in data for item in sublist]
        return np.asarray(data)
        
    def train(self, numOfPerms, alpha=pow(2, -10), clf=None):
        first = False
        if clf == None:
            clf = SGDClassifier(loss='modified_huber', alpha=alpha, class_weight={0:1, 1:5})
            first = True

        data = self.getDataFromPerms(numOfPerms)
        np.random.shuffle(data)
        X = data[:, 3:-1]
        y = data[:, -1]
        y = (y == 1).astype(int) # transforms 3 classes into 2

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

        if first:
            clf.partial_fit(X_train, y_train, np.unique(y))
        else:
            clf.partial_fit(X_train, y_train)

    #     fscore = getF1Score(X_test, y_test, clf)
        y_pred = clf.predict(X_test)
        precision, recall, fscore, support = precision_recall_fscore_support(y_test, y_pred, average='weighted')
        logging.info('Precision: %f, Recall: %f, F-Score: %f' % (precision, recall, fscore))

        return clf  
    
    def generate(self):
        self.clf1 = self.train(numOfPerms=self.num_of_perms)
        for i in range(9):
            self.clf1 = self.train(clf=self.clf1, numOfPerms=self.num_of_perms)
        return self.clf1