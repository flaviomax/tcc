import numpy as np
import math
import pickle
import time
from random import randint
from random import sample
import logging
import lin_decomposition
import re
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

class FeatureGenerator:
    # def _init__(self, perm=None):
    #     self.perm = perm

    def get_normalized_perm(self, perm):
        return [0] + perm + [len(perm) + 1]

    def nof_breakpoints_rev(self, g_perm):
        perm = self.get_normalized_perm(g_perm)
        bp = 0
        n = len(perm)
        for i in range(n-1):
            if(perm[i] - perm[i+1] != 1 and perm[i] - perm[i+1] != -1):
                bp += 1
        return bp

    def nof_breakpoints_trans(self, g_perm):
        perm = self.get_normalized_perm(g_perm)
        bp = 0
        n = len(perm)
        for i in range(n-1):
            if(perm[i] + 1 != perm[i + 1]):
                bp += 1
        return bp

    def get_strips_info(self, g_perm):
        perm = g_perm
        n = len(perm)
        tam_strip = 0
        tam_maior = 0
        n_strips = 0
        sum_tam_strips = 0
        n_strips_crescente = 0
        n_strips_decrescente = 0
        sum_tam_strips_crescente = 0
        sum_tam_strips_decrescente = 0
        i = 0
        while i < n-1:
            # crescent
            if perm[i+1] == perm[i] + 1 and i < n:
                comeco_strip = i
                while i < n - 1 and perm[i+1] == perm[i] + 1:
                    i += 1

                tam_strip = i - comeco_strip + 1
                if tam_strip > tam_maior:
                    tam_maior = tam_strip
                n_strips += 1
                sum_tam_strips = sum_tam_strips + tam_strip
                n_strips_crescente += 1
                sum_tam_strips_crescente = sum_tam_strips_crescente + tam_strip

            # decrescent
            elif perm[i+1] == perm[i]-1 and i < n: 
                comeco_strip = i
                while  i < n - 1 and perm[i+1] == perm[i]-1:
                    i += 1
                tam_strip = i-comeco_strip + 1
                if tam_strip > tam_maior:
                    tam_maior = tam_strip
                n_strips += 1
                sum_tam_strips = sum_tam_strips + tam_strip
                n_strips_decrescente += 1
                sum_tam_strips_decrescente = sum_tam_strips_decrescente + tam_strip
            
            i += 1

        return [tam_maior,
                n_strips,
                sum_tam_strips,
                n_strips_crescente,
                n_strips_decrescente,
                sum_tam_strips_crescente,
                sum_tam_strips_decrescente]

    def get_correct_position(self, g_perm):
        perm = g_perm
        a = np.asarray(perm)
        identity = np.asarray(list(range(1, len(perm) + 1)))
        return np.sum(a == identity)

    def get_fixed_elements(self, g_perm):
        perm = self.get_normalized_perm(g_perm)
        n = len(perm)
        maior = 0
        count = 0

        for i in range(1, n-1):
            if (perm[i] == i):
                if (perm[i] > maior):
                    count += 1

            if (maior < perm[i]):
                maior = perm[i]

        return count

    def get_inversions(self, g_perm):
        def merge(arr, temp, left, mid, right):
            inv_count = 0

            i = left
            j = mid
            k = left

            while (i <= mid - 1) and (j <= right):
                if (arr[i] <= arr[j]):
                    temp[k] = arr[i]
                    k += 1
                    i += 1
                else:
                    temp[k] = arr[j]
                    k += 1
                    j += 1
                    inv_count = inv_count + (mid - i)

            while i <= mid - 1:
                temp[k] = arr[i]
                k += 1
                i += 1

            while j <= right:
                temp[k] = arr[j]
                k += 1
                j += 1

            for i in range(left, right + 1):
                arr[i] = temp[i]

            return inv_count


        def _merge_sort(arr, temp, left, right):
            mid = None
            inv_count = 0
            if (right > left):
                mid = int(math.floor((right + left)/2));

                inv_count  = _merge_sort(arr, temp, left, mid);
                inv_count += _merge_sort(arr, temp, mid+1, right);

                inv_count += merge(arr, temp, left, mid+1, right);
            
            return inv_count;

        def merge_sort(arr, temp, array_size):
            return _merge_sort(arr, temp, 0, array_size - 1)

        perm = self.get_normalized_perm(g_perm)
        arr = list(perm) # makes copy to prevent in-place modification
        array_size = len(arr)
        temp = np.zeros(array_size)

        return merge_sort(arr, temp, array_size)

    def get_prefix(self, g_perm):
        perm = g_perm
        n = len(perm)

        cond = True
        count = 0

        for i in range(0, n):
            if not cond:
                break
            if perm[i] == i + 1:
                count += 1
            else:
                cond = False

        return count

    def get_suffix(self, g_perm):
        perm = self.get_normalized_perm(g_perm)
        cond = True
        count = 0
        n = len(perm)
        i = n - 2
        
        while i > 0 and cond:
            if perm[i] == i:
                count += 1
            else:
                cond = False
            i -= 1

        return count

    def lis_calc(self, perm, n):
        output = np.zeros(n)
        j = None
        lo = None
        hi = None
        mid = None 
        pos = None
        L = None
        M = np.zeros(n + 1).astype(int)
        P = np.zeros(n + 1).astype(int)
        L = 0

        for i in range(1, n):
            if L == 0 or perm[M[1]] >= perm[i]:
                j = 0
            else:
                lo = 1
                hi = L + 1
                while (lo < (hi - 1)):
                    if (lo+hi) % 2 == 0:
                        mid = int(math.floor((lo + hi)/2))
                    else:
                        mid = int(math.floor((lo + hi - 1)/2))
                    if perm[M[mid]] < perm[i]:
                        lo = mid
                    else:
                        hi = mid
                
                j = lo

            P[i] = M[j]
            if j == L or perm[i] < perm[M[j+1]]:
                M[j+1] = i
                if (L < j+1):
                    L = j+1
            
        i = 1
        pos = M[L]
        while L > 0:
            # print pos
            output[i] = perm[pos]
            i += 1
            pos = P[pos]
            L -= 1

        output[0] = i-1
        return int(output[0])

    def get_lis(self, g_perm):
        perm = self.get_normalized_perm(g_perm)
        n = len(perm)
        return self.lis_calc(perm, n) - 1 # changed + for -

    def get_lds(self, g_perm):
        def reversal(perm, i, j):
            for k in range(i, j-i/2 + 1):
                aux = perm[k]
                perm[k] = perm[j - k + i]
                perm[j - k + i] = aux

        perm = self.get_normalized_perm(g_perm)
        n = len(perm)
        perm = list(reversed(perm))
        return self.lis_calc(perm, n)

    def get_entropy(self, g_perm):
        def get_position(perm, n):
            position = [0] * n
            for i in range(0, n):
                position[perm[i]] = i

            return position

        def get_entro(position, n):
            total = 0
            for i in range(0, n):
                total += abs(i - position[i])

            return total

        perm = self.get_normalized_perm(g_perm)
        n = len(perm)
        return get_entro(get_position(perm, n), n)

    def get_left_right_code(self, g_perm):
        def coding(perm, n, type_):
            coding_array = np.zeros(n).astype(int)
            if type_ == 'left':
                for i in range(2, n-1):
                    for j in range(1, i):
                        if perm[i] < perm[j]:
                            coding_array[i] += 1

            elif type_ == 'right':
                for i in range(1, n-2):
                    for j in range(i + 1, n-1):
                        if perm[i] > perm[j]:
                            coding_array[i] += 1

            else:
                raise Exception('TypeError')

            return coding_array

        def coding_value(coding_array, n):
            ca_value = 0
            i = 0

            ca_aux = coding_array[1]
            if ca_aux != 0:
                ca_value = 1
            else:
                ca_value = 0
            for i in range(2, n-1):
                if (coding_array[i] != ca_aux):
                    ca_aux = coding_array[i]
                    if ca_aux > 0:
                        ca_value += 1

            return ca_value

        perm = self.get_normalized_perm(g_perm)
        n = len(perm)
        right = coding_value(coding(perm, n, 'right'), n)
        left = coding_value(coding(perm, n, 'left'), n)

        return left, right

    def get_cycles(self, g_perm):
        def get_position(permutation) : 
            n = len(permutation)-2
            position    = [-1 for i in range(0, n+2)]
            for i in range(0, n+2) :
                position[abs(permutation[i])] = i
            return position

        def get_rightmost_element(cycle, position) :
            max_position = 0
            for i in range(len(cycle)) :
                if position[cycle[i]] > position[cycle[max_position]] :
                    max_position = i
            return max_position

        ## The unordered cycle starts with a gray edge, we order them by
        ## making it start with the rightmost black edge.
        def order_cycle(cycle, position) :
            index = get_rightmost_element(cycle, position)
            new   = [] 
            new.append(cycle[index])

            if index % 2 == 0 :    
                iter_el  = (index-1) % len(cycle)
                while iter_el != index :
                    new.append(cycle[iter_el])
                    iter_el = (iter_el-1) % len(cycle)
            else :
                iter_el  = (index+1) % len(cycle)
                while iter_el != index :
                    new.append(cycle[iter_el])
                    iter_el = (iter_el+1) % len(cycle)
            return new

        ## This will transform this cycle representation in the other one that
        ## numbers the black edges. This will simplify the transformation into
        ## simple permutation since we will use the cycle graph linked
        ## structure.
        def canonical_representation(cycle, position) :
            cycle     = order_cycle(cycle, position)
            canonical = []
            
            for i in range(0,len(cycle),2) :
                if position[cycle[i]] < position[cycle[i+1]] :
                    black = -position[cycle[i+1]]
                    canonical.append(black )
                else : 
                    black = position[cycle[i]]
                    canonical.append(black)
            return canonical
            
        def get_canonicals(str_permutation, int_position):
            canonicals   = [] 
            int_cycles = lin_decomposition.run(str_permutation)
            for cycle in int_cycles :
                canonicals.append(canonical_representation(cycle, int_position))

            return canonicals

        n = len(g_perm)
        str_permutation = str(g_perm)[1:-1]
        permutation = self.get_normalized_perm(g_perm)
        int_position = get_position(permutation)

        
        #str_cycles = get_canonicals(str_permutation, int_position)
        #print str_cycles
        can = get_canonicals(str_permutation, int_position)
        num_ciclos = len(can)
        num_ciclos_impares = 0
        num_ciclos_curtos = 0
        num_ciclos_pares_div = 0
        num_ciclos_orientados = 0
        num_ciclos_unitarios = 0
        tamanho_maior_ciclo = 1
        
        for cycles in can:
            if len(cycles) % 2 != 0:
                num_ciclos_impares = num_ciclos_impares+1
            if len(cycles) < 4:
                num_ciclos_curtos = num_ciclos_curtos+1
            if len(cycles) == 2 and cycles[0] * cycles[1] < 0:
                num_ciclos_pares_div = num_ciclos_pares_div + 1
            if len(cycles) > 2:
                found_oriented = 0
                for i in range(1,len(cycles)-1):
                    if not found_oriented:
                        for j in range(i+1,len(cycles)):
                            if not found_oriented:
                                if cycles[i] > 0 and cycles[j] > 0:
                                    if cycles[j] > cycles[i]:
                                        num_ciclos_orientados = num_ciclos_orientados+1
                                        found_oriented = 1
                                elif cycles[i] < 0 and cycles[j] < 0:
                                    if cycles[j] > cycles[i]:
                                        num_ciclos_orientados = num_ciclos_orientados+1
                                        found_oriented = 1
                                else:
                                    if abs(cycles[j]) > abs(cycles[i]):
                                        num_ciclos_orientados = num_ciclos_orientados+1
                                        found_oriented = 1        
            if len(cycles) == 1:
                num_ciclos_unitarios = num_ciclos_unitarios+1
            if len(cycles) > tamanho_maior_ciclo:
                tamanho_maior_ciclo = len(cycles)
            #falta componentes
            #falta maior componente
        can2 = [s for s in can if len(s) > 1]
        
        num_of_components = len(can) - len(can2)
        biggest_component = 1
        biggest_cycles_component = 1

        while len(can2) > 0:
            current_component = []
            first = can2.pop(0)
            current_component.append(first)
            
            lenght_current_component = len(first)
            num_of_components = num_of_components + 1
            cycles_component = 1
            
            while len(current_component) > 0:
                atual = current_component.pop(0)
                for i in range(-1,len(atual)-1):
                    j = 0
                    while j < len(can2):
                        removed = 0
                        for k in range(-1,len(can2[j])-1):
                            a = min(abs(atual[i]),abs(atual[i+1]))
                            b = max(abs(atual[i]),abs(atual[i+1]))
                            c = min(abs(can2[j][k]),abs(can2[j][k+1]))
                            d = max(abs(can2[j][k]),abs(can2[j][k+1]))
                            
                            if ((c > a) and (b > c) and (d > b)) or ((a > c) and (d > a) and (b > d)):
                                cycles_component = cycles_component + 1
                                lenght_current_component = lenght_current_component + len(can2[j])
                                current_component.append(can2.pop(j))
                                removed = 1
                                break
                        if not removed:
                            j = j + 1 
            
            if (cycles_component > biggest_cycles_component):
                biggest_cycles_component = cycles_component
            
            if (lenght_current_component > biggest_component):
                biggest_component = lenght_current_component

        return [num_ciclos,
               num_ciclos_impares,
               num_ciclos_curtos,
               num_ciclos_pares_div,
               num_ciclos_orientados,
               num_ciclos_unitarios,
               tamanho_maior_ciclo,
               num_of_components,
               biggest_component,
               biggest_cycles_component]

    def get_tuples(self, result):
        keys = [
            'breakpoints reversao',
            'breakpoints trans',
            '01 + 02',
            'tamanho maior strip nu',
            'numero de strip nu',
            'soma dos tamanhos das strips nu',
            'strips crescentes nu',
            'strips decrescentes nu',
            'soma tamanho strips crescentes nu',
            'soma tamanho strips decrescentes nu',
            'elementos na posicao correta',
            'elementos fixos',
            'inversoes',
            'prefixo correto',
            'sufixo correto',
            'lis',
            'lds',
            'entropia',
            'left code',
            'right code',
            'numero de ciclos',
            'ciclos impares',
            'ciclos curtos',
            'ciclos pares divergente',
            'ciclos orientados',
            'ciclos unitarios',
            'maior ciclo',
            'numero de componentes',
            'tamanho da maior componente em numero de arestas',
            'tamanho da maior componente em numero de ciclos'
        ]

        data = []
        for i in range(0, len(keys)):
            data.append((keys[i], result[i]))

        return data

    def run(self, perm):
        result = []
        result.append(self.nof_breakpoints_rev(perm))
        result.append(self.nof_breakpoints_trans(perm))
        result.append(result[0] + result[1])
        result += self.get_strips_info(perm)
        result.append(self.get_correct_position(perm))
        result.append(self.get_fixed_elements(perm))
        result.append(self.get_inversions(perm))
        result.append(self.get_prefix(perm))
        result.append(self.get_suffix(perm))
        result.append(self.get_lis(perm))
        result.append(self.get_lds(perm))
        result.append(self.get_entropy(perm))
        result += self.get_left_right_code(perm)
        result += self.get_cycles(perm)

        return result

    def run_tuples(self, perm):
        return self.get_tuples(self.run(perm))
    
if __name__ == '__main__':
    # a = [1,3,4,2,5,7,6,9,8,10]
    # a = [10, 3, 4, 5, 9, 8, 6, 7, 1, 2]
    a = [10, 9, 8, 3, 4, 5, 6, 7, 1, 2]
    b = FeatureGenerator()

    # print b.nof_breakpoints_rev(a)
    # print b.nof_breakpoints_trans(a)

    c = b.run(a)
    for d in c:
        print d
    # print len(c)


