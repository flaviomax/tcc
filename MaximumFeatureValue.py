import numpy as np


class MaximumFeatureValue:
    def __init__(self, perm_size):
        self.perm_size = perm_size

    def get_maximums(self):
        result = np.zeros(30).astype(int)
        result[[3, 5, 8, 9, 10, 11, 13, 14, 15, 16]] = self.perm_size
        result[[0, 1, 20, 21, 22, 25, 26, 27, 28]] = self.perm_size + 1
        result[[4, 6, 7, 23, 29]] = self.perm_size / 2
        result[2] = 2 * (self.perm_size + 1)
        result[12] = self.perm_size * (self.perm_size - 1) / 2
        result[17] = self.perm_size * self.perm_size / 2
        result[[18, 19]] = self.perm_size - 1
        result[24] = self.perm_size / 3

        return result.astype(np.float)

if __name__ == '__main__':
    a = MaximiumFeatureValue(10)

    b =  a.get_maximums()


    for i, j in enumerate(b):
        print str(i) + ': ' + str(j)