from itertools import combinations
from FeatureGenerator import FeatureGenerator
from MaximumFeatureValue import MaximumFeatureValue
import numpy as np

class OperationsGenerator:
    def __init__(self, perm_size):
        self.perm_size = perm_size
        identity = list(range(0, self.perm_size))
        self.reversion_operations = list(combinations(identity, 2))
        identity = list(range(0, self.perm_size + 1))
        self.transposition_operations = list(combinations(identity, 3))
        self.num_of_features = 30

        self.feature_generator = FeatureGenerator()
        self.maximums = MaximumFeatureValue(self.perm_size).get_maximums()

        self.operations = self.reversion_operations + self.transposition_operations

    def reverse_sublist(self, lst, start, end):
        lst[start:end+1] = lst[start:end+1][::-1]
        return lst

    def get_feature_matrix(self, perm):
        result = np.zeros((len(self.reversion_operations) + len(self.transposition_operations), self.num_of_features))
        initial_features = np.array(self.feature_generator.run(perm)).astype(np.float)
        target_perms = []

        for i, operation in enumerate(self.reversion_operations):
            local_perm = list(perm)
            self.reverse_sublist(local_perm, operation[0], operation[1])
            target_perms.append(local_perm)
            final_features = np.array(self.feature_generator.run(local_perm)).astype(np.float)
            feature_vector = np.divide(final_features - initial_features, self.maximums)

            result[i, :] = feature_vector

        for i, operation in enumerate(self.transposition_operations):
            aux = list(perm)
            local_perm = aux[0:operation[0]] + aux[operation[1]:operation[2]] + aux[operation[0]:operation[1]] + aux[operation[2]:]
            target_perms.append(local_perm)
            final_features = np.array(self.feature_generator.run(local_perm)).astype(np.float)
            feature_vector = np.divide(final_features - initial_features, self.maximums)

            result[i + len(self.reversion_operations), :] = feature_vector

        return result, target_perms


if __name__ == '__main__':
    a =  OperationsGenerator(10)

    # b, c = a.get_feature_matrix([2, 3, 4, 5, 6, 7, 8, 9, 10, 1])

    # # print a.reversion_operations
    # # print a.transposition_operations
    # print len(c)
    # print c

    maximums = a.maximums
    v1 = np.array([6,
            7,
            13,
            3,
            4,
            9,
            3,
            1,
            7,
            2,
            0,
            0,
            28,
            0,
            0,
            5,
            5,
            36,
            4,
            5,
            7,
            5,
            6,
            0,
            1,
            5,
            4,
            6,
            6,
            2], dtype=np.float)

    v2 = np.array([4,
            6,
            10,
            5,
            3,
            10,
            2,
            1,
            7,
            3,
            0,
            0,
            34,
            0,
            0,
            5,
            5,
            42,
            4,
            4,
            8,
            7,
            7,
            0,
            1,
            7,
            4,
            8,
            4,
            1], dtype=np.float)

    feature_vector = np.divide(v2 - v1, maximums)
    for item in feature_vector:
        print "{0:.2f}".format(item)