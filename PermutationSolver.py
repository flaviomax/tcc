import numpy as np
import math
import pickle
from multiprocessing import Pool
import time
import os, argparse
from random import randint
from random import sample
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.metrics import precision_recall_fscore_support
import logging
from ClassifierGenerator import ClassifierGenerator
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

base = '/home/flavio/unicamp/tcc/resultFeatures3Classes/'

class PermutationSolver:
    def __init__(self, initial_perm_code=None, perm_size=10, clf=None):
        self.initial_perm_code = initial_perm_code
        self.clf = clf
        self.perm_size = perm_size
        self.base = base
        
    def unrank(self, n, r, current_perm):
        if n > 0:
            current_perm[n-1], current_perm[r % n] = current_perm[r % n], current_perm[n-1]
            self.unrank(n-1, int(r/n), current_perm)
    
    def decod_perm(self, perm_code):
        '''
        @returns: list with permutation
        '''
        n = self.perm_size
        result = list(range(1, n + 1))
        self.unrank(n, perm_code - 1, result)
        return result
    
    def load_permutation_data(self, perm_code):
        result = np.loadtxt(self.base + str(perm_code).zfill(7), delimiter=',')
        return result
    
    def classify_possible_permutations(self, current_perm_code=None):
        if current_perm_code == None and self.initial_perm_code != None:
            current_perm_code = self.initial_perm_code
        if current_perm_code == None:
            logging.error('Cant find current_perm_code')
            raise Exception('NullCode')

        if current_perm_code == math.factorial(self.perm_size):
            return current_perm_code
        perm_data = self.load_permutation_data(current_perm_code)
        target_perm_codes = perm_data[:, 2]
        operations_codes = perm_data[:, 1]
        feature_vectors = perm_data[:, 3:-1]
        y = perm_data[:, -1]
        
        improve_prob = clf.predict_proba(feature_vectors)
#         print improve_prob
        best_op = int(np.argmax(improve_prob[:, 1]))
        logging.info('Chose %s as best new permutation, with %2f confidence' % (
                    str(self.decod_perm(int(target_perm_codes[best_op]))),
                    np.max(improve_prob)
            )
        )
        if y[best_op] == 1:
            logging.info('It was a good decision! Distance was improved')
        elif y[best_op] == 0:
            logging.info('It was a neutral decision. Distance kept.')
        else:
            logging.info('''It was a BAD decision :( Distance increased''')
        return int(target_perm_codes[best_op])
    
    def run(self):
        logging.info('Starting run for permutation %s' %
                    str(self.decod_perm(self.initial_perm_code)))
        rnd = 0
        current_perm = None
        while rnd < 30:
            rnd += 1
            new_perm = self.classify_possible_permutations(current_perm)
            if new_perm != math.factorial(self.perm_size): #if it is not the solution
                current_perm = new_perm
            else:
                logging.info('Solution found in %d steps', rnd)
                break

            if rnd == 30:
                logging.info('''Solution could not be found :(''')

if __name__ == '__main__':
    # perm_code, gen_needed = parse_args(sys.argv[1:])
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', action='store_true', help='generate new classifier')
    parser.add_argument('-p', nargs='?', help='permutation code')
    # parser.print_help()
    args = parser.parse_args()
    try:
        perm_code = int(args.p)
    except:
        parser.print_help()
        exit()

    gen_needed = args.g

    if gen_needed:
        logging.info('Generating Classifier...')
        gen = ClassifierGenerator(base=base)
        clf = gen.generate() # here we get the classifier
        pickle.dump(clf, open('classifier.pickle', 'w'))
        logging.info('Classifier generated successfully.')

    else:
        try:
            clf = pickle.load(open('classifier.pickle', 'r'))
        except IOError:
            logging.error('Could not find classifier.'
                         +'Please run with -g argument to create classifier')
            exit()

    logging.info('Starting permutation solver...')

    perm_solver = PermutationSolver(perm_code)
    perm_solver.run()

    logging.info('Done!')