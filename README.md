# Projeto Final de Graduação
## Institute of Computing, University of Campinas
### Flavio Altinier Maximiano da Silva

Directories structure:
    
    root: all source files to perform experiments
    gera-featur: C code files to generate permutation features. All of those were converted to python in /FeatureGenerator.py
    perm2code: permutations are usually represented by codes. Files here convert between formats.
    report: all .tex files necessary to generate final report
    
Usage:

    1. Generate classifier model with `python PermutationSolver.py -g`
    2. Run experiments with `python SolverTest.py`.