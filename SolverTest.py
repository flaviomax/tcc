from AnyPermutationSolver import AnyPermutationSolver
import numpy as np
import math
import pickle
import time
import os, argparse
from random import shuffle
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.metrics import precision_recall_fscore_support
import logging
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

class SolverTest:
    def __init__(self, perm_size):
        self.perm_size = perm_size

    def perm_to_str(self, perm):
        return ','.join(map(str, perm))

    def operations_to_str(self, operations):
        ops = []
        for op in operations:
            items = []
            for item in op:
                items.append(item+1)
            ops.append('(%s)' % ','.join(map(str, items)))
        return '[%s]' % ','.join(ops)

    def run(self, num_of_tests, output_file):
        with open(output_file, 'wb') as f:
            for i in range(num_of_tests):
                current_perm = list(range(1, self.perm_size + 1))
                shuffle(current_perm)
                solver = AnyPermutationSolver(current_perm, len(current_perm))
                rounds, operations = solver.run()
                # f.write(str(current_perm) + '|' + str(rounds) + '\n')
                f.write('%s %d %s\n' % (self.perm_to_str(current_perm),
                                    rounds, self.operations_to_str(operations)))

if __name__ == '__main__':
    a = SolverTest(20)

    a.run(10, 'solver_run_2.csv')

