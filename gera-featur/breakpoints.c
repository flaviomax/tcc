#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int nof_breakpoints_rev(int *p, int n) {
    int i, bp = 0;
    for (i = 0; i < n-1; i++)
        if (p[i] - p[i+1] != 1 && p[i] - p[i+1] != -1)
            bp++;
    return bp;
}


int nof_breakpoints_trans(int *p, int n) {
    int i, bp = 0;
    for (i = 0; i < n-1; i++)
        if (p[i] +1 != p[i+1])
            bp++;
    return bp;
}


void get_permutation(char strprm[], int *p) {
    char *r = NULL;
    int i;

    r = strtok(strprm, ",");
    i = 1;
    while (r != NULL) {
        p[i++] = atoi(r);
        r = strtok(NULL, ",");
    }
    p[0] = 0;
    p[i] = i;
}


int main(int argc, char *argv[]) {
    int n, *perm;
    char op;
    
    n = atoi(argv[1]);
    n = n+2;
    op = argv[3][0];
    
    perm = malloc(sizeof(int)*n);
       
    get_permutation(argv[2], perm);

    if(op == 'r')
        printf("%d\n", nof_breakpoints_rev(perm, n));
    else if(op == 't')
        printf("%d\n", nof_breakpoints_trans(perm, n));
    else
        printf("invalid operation\n");


    free(perm);
    return 0;
}