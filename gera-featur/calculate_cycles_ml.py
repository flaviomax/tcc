import sys
import commands
from subprocess import call
import lin_decomposition

#######################################################
## Functions applied on cycles
#######################################################
def get_position(permutation) : 
    n = len(permutation)-2
    position    = [-1 for i in range(0, n+2)]
    for i in range(0, n+2) :
        position[abs(permutation[i])] = i
    return position

def get_rightmost_element(cycle, position) :
    max_position = 0
    for i in range(len(cycle)) :
        if position[cycle[i]] > position[cycle[max_position]] :
            max_position = i
    return max_position

## The unordered cycle starts with a gray edge, we order them by
## making it start with the rightmost black edge.
def order_cycle(cycle, position) :
    index = get_rightmost_element(cycle, position)
    new   = [] 
    new.append(cycle[index])

    if index % 2 == 0 :    
        iter_el  = (index-1) % len(cycle)
        while iter_el != index :
            new.append(cycle[iter_el])
            iter_el = (iter_el-1) % len(cycle)
    else :
        iter_el  = (index+1) % len(cycle)
        while iter_el != index :
            new.append(cycle[iter_el])
            iter_el = (iter_el+1) % len(cycle)
    return new

## This will transform this cycle representation in the other one that
## numbers the black edges. This will simplify the transformation into
## simple permutation since we will use the cycle graph linked
## structure.
def canonical_representation(cycle, position) :
    cycle     = order_cycle(cycle, position)
    canonical = []
    
    for i in range(0,len(cycle),2) :
        if position[cycle[i]] < position[cycle[i+1]] :
            black = -position[cycle[i+1]]
            canonical.append(black )
        else : 
            black = position[cycle[i]]
            canonical.append(black)
    return canonical
	
def get_canonicals(str_permutation, int_position):
    canonicals   = []
    int_cycles = lin_decomposition.run(str_permutation)
    for cycle in int_cycles :
    	canonicals.append(canonical_representation(cycle, int_position))
    return canonicals

if __name__ == "__main__" :
    str_permutation = sys.argv[1]
    permutation = eval("[%s]" % str_permutation)
    n = len(permutation)
    permutation = [0] + permutation + [n+1]

    int_position = get_position(permutation)
    
    #str_cycles = get_canonicals(str_permutation, int_position)
    #print str_cycles
    can = get_canonicals(str_permutation, int_position)
    num_ciclos = len(can)
    num_ciclos_impares = 0
    num_ciclos_curtos = 0
    num_ciclos_pares_div = 0
    num_ciclos_orientados = 0
    num_ciclos_unitarios = 0
    tamanho_maior_ciclo = 1
    
    for cycles in can:
        if len(cycles) % 2 != 0:
            num_ciclos_impares = num_ciclos_impares+1
        if len(cycles) < 4:
            num_ciclos_curtos = num_ciclos_curtos+1
        if len(cycles) == 2 and cycles[0] * cycles[1] < 0:
            num_ciclos_pares_div = num_ciclos_pares_div + 1
        if len(cycles) > 2:
            found_oriented = 0
            for i in range(1,len(cycles)-1):
                if not found_oriented:
                    for j in range(i+1,len(cycles)):
                        if not found_oriented:
                            if cycles[i] > 0 and cycles[j] > 0:
                                if cycles[j] > cycles[i]:
                                    num_ciclos_orientados = num_ciclos_orientados+1
                                    found_oriented = 1
                            elif cycles[i] < 0 and cycles[j] < 0:
                                if cycles[j] > cycles[i]:
                                    num_ciclos_orientados = num_ciclos_orientados+1
                                    found_oriented = 1
                            else:
                                if abs(cycles[j]) > abs(cycles[i]):
                                    num_ciclos_orientados = num_ciclos_orientados+1
                                    found_oriented = 1        
        if len(cycles) == 1:
            num_ciclos_unitarios = num_ciclos_unitarios+1
        if len(cycles) > tamanho_maior_ciclo:
            tamanho_maior_ciclo = len(cycles)
        #falta componentes
        #falta maior componente
    can2 = [s for s in can if len(s) > 1]
    
    num_of_components = 0
    biggest_component = 0
    biggest_cycles_component = 0

    while len(can2) > 0:
        current_component = []
        first = can2.pop(0)
        current_component.append(first)
        
        lenght_current_component = len(first)
        num_of_components = num_of_components + 1
        cycles_component = 1
        
        while len(current_component) > 0:
            atual = current_component.pop(0)
            for i in range(-1,len(atual)-1):
                j = 0
                while j < len(can2):
                    removed = 0
                    for k in range(-1,len(can2[j])-1):
                        a = min(abs(atual[i]),abs(atual[i+1]))
                        b = max(abs(atual[i]),abs(atual[i+1]))
                        c = min(abs(can2[j][k]),abs(can2[j][k+1]))
                        d = max(abs(can2[j][k]),abs(can2[j][k+1]))
                        
                        if ((c > a) and (b > c) and (d > b)) or ((a > c) and (d > a) and (b > d)):
                            cycles_component = cycles_component + 1
                            lenght_current_component = lenght_current_component + len(can2[j])
                            current_component.append(can2.pop(j))
                            removed = 1
                            break
                    if not removed:
                        j = j + 1 
        
        if (cycles_component > biggest_cycles_component):
            biggest_cycles_component = cycles_component
        
        if (lenght_current_component > biggest_component):
            biggest_component = lenght_current_component
        


                
    
    print num_ciclos, num_ciclos_impares, num_ciclos_curtos, num_ciclos_pares_div, num_ciclos_orientados, num_ciclos_unitarios, tamanho_maior_ciclo, num_of_components, biggest_component, biggest_cycles_component
    
    
