#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void get_position(int *perm, int *position, int n) {
    int i;
    
    for (i = 0; i < n; i++)
        position[perm[i]] = i;
}

int get_entro(int *position, int n) {
    int i, total = 0;
    for (i = 0; i < n; i++)
        total = total + abs(i - position[i]);
    return total;
}


void get_permutation(char strprm[], int *p) {
    char *r = NULL;
    int i;

    r = strtok(strprm, ",");
    i = 1;
    while (r != NULL) {
        p[i++] = atoi(r);
        r = strtok(NULL, ",");
    }
    p[0] = 0;
    p[i] = i;
}


int main(int argc, char *argv[]) {
    int n, *perm, *position;
    
    n = atoi(argv[1]);
    n = n+2;
    
    perm = malloc(sizeof(int)*n);
    position = malloc(sizeof(int)*n);
       
    get_permutation(argv[2], perm);

    get_position(perm, position, n);
    
    printf("%d\n", get_entro(position, n));

    free(perm);
    free(position);
    return 0;
}
