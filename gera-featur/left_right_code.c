#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEFT 0
#define RIGHT 1

void coding(int *perm, int *coding_array, int n, int type) {
    int i, j;
    
    for (i = 1; i < n-1; i++)
        coding_array[i] = 0;
    
    if (type == LEFT) {
        for (i = 2; i < n-1; i++)
            for (j = 1; j < i; j++)
                if (perm[i] < perm[j])
                    coding_array[i]++;
    
    }
    else {
        for (i = 1; i < n-2; i++)
            for (j = i+1; j < n-1; j++)
                if (perm[i] > perm[j])
                    coding_array[i]++;
    }

}

int coding_value(int *coding_array, int n) {
    int ca_aux, ca_value, i;
    
    ca_aux = coding_array[1];
    if (ca_aux != 0)
        ca_value = 1;
    else
        ca_value = 0;
    for (i = 2; i < n-1; i++) {
        if (coding_array[i] != ca_aux) {
            ca_aux = coding_array[i];
            if (ca_aux > 0)
                ca_value++;
        }
    }
    
    return ca_value;

}


void get_permutation(char strprm[], int *p) {
    char *r = NULL;
    int i;

    r = strtok(strprm, ",");
    i = 1;
    while (r != NULL) {
        p[i++] = atoi(r);
        r = strtok(NULL, ",");
    }
    p[0] = 0;
    p[i] = i;
}


int main(int argc, char *argv[]) {
    int n, *perm, *coding_array;
    char type;
    
    n = atoi(argv[1]);
    n = n+2;
    type = argv[3][0];
    
    perm = malloc(sizeof(int)*n);
    coding_array = malloc(sizeof(int)*n);
       
    get_permutation(argv[2], perm);

    if(type == 'r') {
        coding(perm, coding_array, n, RIGHT);
        printf("%d\n", coding_value(coding_array, n));
    }
    else if(type == 'l') {
        coding(perm, coding_array, n, LEFT);
        printf("%d\n", coding_value(coding_array, n));
    }
    else
        printf("invalid type\n");


    free(perm);
    free(coding_array);
    return 0;
}
