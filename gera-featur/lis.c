#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* retorna uma lista da maior lis onde o tamanho da mesma
   esta em output[0] */
int *LiS(int *perm, int n, int *output) {
    int i, j, lo, hi, mid, pos, L, *M, *P;
    M = malloc(sizeof(int) * n+1);
    P = malloc(sizeof(int) * n+1);
    L = 0;
    // printf("%d\n", n);
    for (i = 1; i < n; i++) {
        if (L == 0 || perm[M[1]] >= perm[i]) {
            /* nao existe j restrito a perm[M[1]] < perm[i] */
            j = 0;
        }
        else {
            /*busca binaria pelo maior j restrito a perm[M[j]] < perm[i] */
            lo = 1; /* maior valor conhecido <= j */
            hi = L + 1; /* menor valor conhecido > j */
            while (lo < (hi - 1)) {
                if ((lo+hi) % 2 == 0)
                    mid = (lo + hi)/2;
                else
                    mid = (lo + hi - 1)/2;
                if (perm[M[mid]] < perm[i]){
                    lo = mid;
                }
                else
                    hi = mid;
            }
            j = lo;
        }
        P[i] = M[j];
        // printf("%d\n", M[j]);
        if (j == L || perm[i] < perm[M[j+1]]) {
            M[j+1] = i;
            if (L < j+1)
                L = j+1;
        }
    }
    i = 1;
    pos = M[L];

    while (L > 0) {
        output[i++] = perm[pos];
        pos = P[pos];
        L--;
    }
    free(M);
    free(P);
    output[0] = i-1;
    return output;
}

void get_permutation(char strprm[], int *p) {
    char *r = NULL;
    int i;

    r = strtok(strprm, ",");
    i = 1;
    while (r != NULL) {
        p[i++] = atoi(r);
        r = strtok(NULL, ",");
    }
    p[0] = 0;
    p[i] = i;
}


int main(int argc, char *argv[]) {
    int n, *perm, *output;
    
    n = atoi(argv[1]);
    n = n+2;
    
    perm = malloc(sizeof(int)*n);
    output = malloc(sizeof(int)*n);
       
    get_permutation(argv[2], perm);
    
    LiS(perm, n, output);
    
    // printf("%d\n", output[0]-1);
    int i = 0;
    // for (i = 0; i < n; i++){
    //     printf("%d\n", output[i]);
    // }

    free(perm);
    free(output);
    return 0;
}
