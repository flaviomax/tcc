#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int get_correct_position(int *perm, int n) {
    int i, count = 0;
    
    for (i = 1; i < n-1; i++)
        if (perm[i] == i)
            count++;

    return count;
}


void get_permutation(char strprm[], int *p) {
    char *r = NULL;
    int i;

    r = strtok(strprm, ",");
    i = 1;
    while (r != NULL) {
        p[i++] = atoi(r);
        r = strtok(NULL, ",");
    }
    p[0] = 0;
    p[i] = i;
}


int main(int argc, char *argv[]) {
    int n, *perm;
    
    n = atoi(argv[1]);
    n = n+2;
    
    perm = malloc(sizeof(int)*n);
       
    get_permutation(argv[2], perm);
    
    printf("%d\n", get_correct_position(perm, n));

    free(perm);
    return 0;
}
