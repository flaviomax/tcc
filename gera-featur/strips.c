#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int nof_breakpoints_rev(int *p, int n) {
    int i, bp = 0;
    for (i = 0; i < n-1; i++)
        if (p[i] - p[i+1] != 1 && p[i] - p[i+1] != -1)
            bp++;
    return bp;
}


int nof_breakpoints_trans(int *p, int n) {
    int i, bp = 0;
    for (i = 0; i < n-1; i++)
        if (p[i] +1 != p[i+1])
            bp++;
    return bp;
}


void get_permutation(char strprm[], int *p) {
    char *r = NULL;
    int i;

    r = strtok(strprm, ",");
    i = 0;
    while (r != NULL) {
        p[i++] = atoi(r);
        r = strtok(NULL, ",");
    }
}


int main(int argc, char *argv[]) {
    int i = 0, n, *perm, comeco_strip;
    int tam_strip = 0, tam_maior = 0, n_strips = 0, sum_tam_strips = 0;
    int n_strips_crescente = 0, n_strips_decrescente = 0;
    int sum_tam_strips_crescente = 0, sum_tam_strips_decrescente = 0;
    
    n = atoi(argv[1]);
    n = n;
    
    perm = malloc(sizeof(int)*n);
       
    get_permutation(argv[2], perm);
    
    while (i < n-1) {
        /* strip crescente */
        if (perm[i+1] == perm[i]+1 && i < n) {
            comeco_strip = i;
            while (perm[i+1] == perm[i]+1 && i < n)
                i++;
            tam_strip = i-comeco_strip+1;
            if (tam_strip > tam_maior)
                tam_maior = tam_strip;
            n_strips++;
            sum_tam_strips = sum_tam_strips+tam_strip;
            n_strips_crescente++;
            sum_tam_strips_crescente = sum_tam_strips_crescente + tam_strip;
            
        /* strip decrescente */
        } else if (perm[i+1] == perm[i]-1 && i < n) {
            comeco_strip = i;
            while (perm[i+1] == perm[i]-1 && i < n)
                i++;
            tam_strip = i-comeco_strip+1;
            if (tam_strip > tam_maior)
                tam_maior = tam_strip;
            n_strips++;
            sum_tam_strips = sum_tam_strips+tam_strip;
            n_strips_decrescente++;
            sum_tam_strips_decrescente = sum_tam_strips_decrescente + tam_strip;
        }
        i++;
    
    }

    printf("%d %d %d %d %d %d %d\n", tam_maior, n_strips, sum_tam_strips, n_strips_crescente,
       n_strips_decrescente, sum_tam_strips_crescente, sum_tam_strips_decrescente);


    free(perm);
    return 0;
}
