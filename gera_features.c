#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NOF_FEATURES 31

#define NOF_PERMUTATIONS 3628801

void reversal(int *perm, int i, int j) {
    int k, aux;

    for (k = i; k <= (j-i)/2 + i; k++) {
        aux = perm[k];
        perm[k] = perm[j - k + i];
        perm[j - k + i] = aux;
    }

}

void transposition(int *perm, int i, int j, int k) {
    int t, l, seg1, seg2, *aux;

    aux = malloc(sizeof(int) * (j - i + 1));
    seg1 = j - i;
    seg2 = k - j;

    if (seg2 < seg1) {
        for (t = i; t <= seg2 + i - 1; t++) {
            aux[t-i] = perm[t];
            perm[t] = perm[j + t - i];
        }
        l = 0;
        for (t = i + seg2; t < j; t++) {
            aux[t-i] = perm[t];
            perm[t] = aux[l++];
        }
        for (t = j; t < k; t++) {
            perm[t] = aux[l++];
        }
    } else {
        for (t = i; t < j; t++) {
            aux[t-i] = perm[t];
            perm[t] = perm[j + t - i];
        }
        for (t = j; t <= i + seg2 - 1; t++) {
            perm[t] = perm[t + seg1];
        }
        l = 0;
        for (t = i + seg2; t < k; t++) {
            perm[t] = aux[l++];
        }
    }

    free(aux);
}

void decode_op(int tam, long int coded, int is_signed, int *i, int *j, int *k) {
    /* auxiliares */
    int b, init;
    int remove_coded=0;

    if (coded > 0) {
        /* procura uma reversao. inicia os valores de i e j */
        *i = 1;
        *j = 2 - is_signed;
        init = tam - 1 + is_signed;

        /* encontra o valor de i */
        while ((coded > (tam - *i + is_signed)) && (*j <= tam)) {
            (*i)++;
            (*j)++;
            coded = coded - init;
            init--;
        }

        /* se existe valor de j possivel, temos a reversao i,j */
        if ((coded <= tam - *i + is_signed) && (*j <= tam)) {
            *j = coded+*i-is_signed;
            *k = 0;
        }
        else {
            /* senao eh uma transposicao. inicia valores de i e j */
            *i = 1;
            *j = 2;

            for (b = *i; b < tam; b++) {
                init = tam-b;
                remove_coded = remove_coded + init;
            }

            /* encontra o valor de i */
            while(remove_coded < coded && *j <= tam) {
                coded = coded - remove_coded;
                (*i)++;
                (*j)++;
                remove_coded = 0;
                for (b = *i; b < tam; b++) {
                    init = tam-b;
                    remove_coded = remove_coded + init;
                }
            }

            /* encontra valor de j */
            init = tam-*i;
            while (init < coded && *j <= tam && init > 0) {
                (*j)++;
                coded = coded - init;
                init--;
            }
            /* encontra o valor de k */
            if (coded+*j <= tam+1) {
                *k = coded+*j;
            } else {
                *i = 0;
                *j = 0;
                *k = 0;
            }
        }
    }
    else {
        *i = 0;
        *j = 0;
        *k = 0;
    }
}

void swap(int *v, int i, int j){
    int temp;

    temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}

void unrank(int n, long int r, int *pi){
    if(n > 0){
        swap(pi,n-1,r%n);
        unrank(n-1,r/n,pi);
    }
}

long int rank(int n, int *pi, int *invpi){
    int s;

    if(n == 1){
        return 0;
    }

    s = pi[n-1]-1;
    swap(pi,n-1,invpi[n-1]-1);
    swap(invpi,s,n-1);

    return (s + n*rank(n-1,pi,invpi));
}

void createIdentity(int *vector, int n){
    int i;

    for(i = 0; i < n; i++){
        vector[i] = i+1;
    }
}

void getInversePermutation(int *pi, int *invpi, int n){
    int i;

    for(i = 0; i < n; i++){
        invpi[pi[i]-1] = i+1;
    }
}

void fill_all(int **a) {
    int i, j;
    FILE *pFeat, *pDist;

    pFeat = fopen("/home/flavio/unicamp/tcc/all10-features/all","r");
    pDist = fopen("/home/flavio/unicamp/tcc/all10-features/all.dist","r");

    for (i = 1; i < NOF_PERMUTATIONS; i++) {
        for (j = 0; j < (NOF_FEATURES-2); j++)
            fscanf (pFeat, "%d,", &(a[i][j]));
        fscanf (pFeat, "%d\n", &(a[i][j]));
        j++;
        fscanf (pDist, "%d\n", &(a[i][j]));
    }
    fclose(pFeat);
    fclose(pDist);
}

int main(int argc, char* argv[]) {
    int i, j, k;
    int *pi, *invpi;
    int **vet_all;
    int codedperm, aux1, start_point, end_point;
    int aux2, is_signed = 0, tam = 10;
    double *car_delta;
    char strfile[50];
    FILE *pFinal;

    pi = malloc(sizeof(int)*tam);
	invpi = malloc(sizeof(int)*tam);
	car_delta = malloc(sizeof(double)*NOF_FEATURES);
	vet_all = malloc(sizeof(int*)*NOF_PERMUTATIONS);
	for (aux1 = 0; aux1 < NOF_PERMUTATIONS; aux1++)
	    vet_all[aux1] = malloc(sizeof(int)*NOF_FEATURES);

    fill_all(vet_all);

    start_point = atol(argv[1]);
    end_point = atol(argv[2]);

    for (aux1 = start_point; aux1 <= end_point; aux1++) {
        strfile[0] = '\0';
        sprintf(strfile, "/home/flavio/unicamp/tcc/resultFeatures/%07d", aux1);
        pFinal = fopen (strfile,"a");

        for (aux2 = 1; aux2 <= 210; aux2++) {

            createIdentity(pi, tam);
            unrank(tam, aux1-1, pi);
            decode_op(tam, aux2, is_signed, &i, &j, &k);

            if (i*j*k > 0) {
                transposition(pi, i-1, j-1, k-1);
	            getInversePermutation(pi, invpi, tam);
                codedperm = rank(tam, pi, invpi) + 1;
            } else if (i*j > 0) {
                reversal(pi, i-1, j-1);
	            getInversePermutation(pi, invpi, tam);
                codedperm = rank(tam, pi, invpi) + 1;
            }

            for (i = 0; i < 30; i++) {
                car_delta[i] = 1.0*(vet_all[codedperm][i] - vet_all[aux1][i]);
            }

            car_delta[0] = car_delta[0]/11.0;
            car_delta[1] = car_delta[1]/11.0;
            car_delta[2] = car_delta[2]/22.0;
            car_delta[3] = car_delta[3]/10.0;
            car_delta[4] = car_delta[4]/5.0;
            car_delta[5] = car_delta[5]/10.0;
            car_delta[6] = car_delta[6]/5.0;
            car_delta[7] = car_delta[7]/5.0;
            car_delta[8] = car_delta[8]/10.0;
            car_delta[9] = car_delta[9]/10.0;
            car_delta[10] = car_delta[10]/10.0;
            car_delta[11] = car_delta[11]/10.0;
            car_delta[12] = car_delta[12]/45.0;
            car_delta[13] = car_delta[13]/10.0;
            car_delta[14] = car_delta[14]/10.0;
            car_delta[15] = car_delta[15]/10.0;;
            car_delta[16] = car_delta[16]/10.0;
            car_delta[17] = car_delta[17]/50.0;
            car_delta[18] = car_delta[18]/9.0;
            car_delta[19] = car_delta[19]/9.0;
            car_delta[20] = car_delta[20]/11.0;
            car_delta[21] = car_delta[21]/11.0;
            car_delta[22] = car_delta[22]/11.0;
            car_delta[23] = car_delta[23]/5.0;
            car_delta[24] = car_delta[24]/3.0;
            car_delta[25] = car_delta[25]/11.0;
            car_delta[26] = car_delta[26]/11.0;
            car_delta[27] = car_delta[27]/11.0;
            car_delta[28] = car_delta[28]/11.0;
            car_delta[29] = car_delta[29]/5.0;

            fprintf(pFinal, "%d,%d,%d,", aux1, aux2, codedperm);

            for (i = 0; i < 30; i++) {
                fprintf(pFinal, "%.3g,", car_delta[i]);
            }

            if (vet_all[aux1][30] > vet_all[codedperm][30])
                fprintf(pFinal, "1\n");
            else
                fprintf(pFinal, "0\n");
        }

        fclose(pFinal);
    }

    free(pi);
    free(invpi);
    free(car_delta);
    for (aux1 = 0; aux1 < NOF_PERMUTATIONS; aux1++)
	    free(vet_all[aux1]);
	free(vet_all);

    return 0;
}
