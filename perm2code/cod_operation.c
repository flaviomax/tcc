/* Autor: Andre Rodrigues Oliveira
   Email: andrero@ic.unicamp.br
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int rev(int tam, int i, int j, int is_signed) {
    int a, init, coded = 0;

    /* valor de i */
    init = tam - 1 + is_signed;

    for (a = 1; a < i; a++) {
        coded = coded + init;
        init--;
    }

    /* valor de j */
    coded = coded + j - i + is_signed;

    return coded;
}

int trans(int tam, int i, int j, int k, int init_t) {
    int a, b, init, coded = 0;

    /* valor de i */
    for (a = 1; a < i; a++) {
        for (b = a; b < tam; b++) {
            init = tam-b;
            coded = coded + init;
        }
    }

    /* valor de j */
    init = tam-i;

    for (a = i+1; a < j; a++) {
        coded = coded + init;
        init--;
    }

    /* valor de k */
    coded = coded + k - j;
    
    return coded+init_t;
}

int main(int argc, char *argv[]) {
    /* auxiliares */
    int tam, is_signed, op[3], init_t, i;
    char op_type, *r;
    
    if (argc != 4) {
        printf("%s tam_perm operation perm_type\n", argv[0]);
        printf("\t tam_perm  = tamanho da permutacao\n");
        printf("\t operation = operacao 'i,j' para reversao ou 'i,j,k' para transposicao\n");
        printf("\t perm_type = 'u' para permutacao sem sinal e 's' para permutacao com sinal\n");
        fflush(stdout);
        exit(0);
    }
    
    tam = atoi(argv[1]);

    r = strtok(argv[2], "(,)");
    i = 0;
    while (r != NULL) {
        op[i++] = atoi(r);
        r = strtok(NULL, ",");
    }
    
    if (i == 3)
        op_type = 't';
    else
        op_type = 'r';

    if (argv[3][0] == 'u')
        is_signed = 0;
    else
        is_signed = 1;
    

    if (op_type == 'r') {
        printf("%d\n", rev(tam, op[0], op[1], is_signed));
        /*return rev(tam, op[0], op[1], is_signed);*/
    } else {
        init_t = rev(tam, tam - 1 + is_signed, tam, is_signed);
        printf("%d\n", trans(tam, op[0], op[1], op[2], init_t));
        /*return trans(tam, op[0], op[1], op[2], init_t);*/
    }

    return 0;
}
