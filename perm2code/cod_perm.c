#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swap(int *v, int i, int j){
	int temp;

	temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}

void getInversePermutation(int *pi, int *invpi, int n){
	int i;

	for(i = 0; i < n; i++){
		invpi[pi[i]-1] = i+1;
	}
}

unsigned int rank(int n, int *pi, int *invpi){
	int s;

	if(n == 1){
		return 0;
	}

	s = pi[n-1]-1;
	swap(pi,n-1,invpi[n-1]-1);
	swap(invpi,s,n-1);

	return (s + n*rank(n-1,pi,invpi));
}

void read_permutation(char strprm[], int *p) {/*{{{*/
    char *r = NULL;
    int i;

    r = strtok(strprm, ",");
    i = 0;
    while (r != NULL) {
        p[i] = atoi(r);
        i++;
        r = strtok(NULL, ",");
    }
}

int main(int argc, char* argv[]) {
	int n, *pi, *invpi;

    if (argc != 3) {
        printf("%s tam_perm perm\n", argv[0]);
        printf("\t tam_perm  = tamanho da permutacao\n");
        printf("\t perm = permutacao (ex: 1,2,3,4,5)\n");
        fflush(stdout);
        exit(0);
    }

	n = atoi(argv[1]);
	pi = malloc(sizeof(int)*n);
	invpi = malloc(sizeof(int)*n);
	
	read_permutation(argv[2], pi);
	getInversePermutation(pi, invpi, n);
	printf("%d\n", rank(n, pi, invpi)+1);
	return 0;
}
