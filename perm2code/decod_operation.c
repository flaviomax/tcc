/* Autor: Andre Rodrigues Oliveira
   Email: andrero@ic.unicamp.br
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    /* auxiliares */
    int tam, is_signed, i, j, b, coded, init;
    int remove_coded=0;

    if (argc != 4) {
        printf("%s tam_perm operation_code perm_type\n", argv[0]);
        printf("\t tam_perm       = tamanho da permutacao\n");
        printf("\t operation_code = codigo da operacao, um numero inteiro i maior que 1\n");
        printf("\t perm_type      = caractere 'u' para permutacao sem sinal e caractere 's' para permutacao com sinal\n");
        fflush(stdout);
        exit(0);
    }

    tam = atoi(argv[1]);

    coded = atoi(argv[2]);

    if (argv[3][0] == 'u')
        is_signed = 0;
    else
        is_signed = 1;

    if (coded > 0) {

        /* procura uma reversao. inicia os valores de i e j */
        i = 1;
        j = 2 - is_signed;
        init = tam - 1 + is_signed;

        /* encontra o valor de i */
        while ((coded > (tam - i + is_signed)) && (j <= tam)) {
            i++;
            j++;
            coded = coded - init;
            init--;
        }

        /* se existe valor de j possivel, imprime a reversao i,j */
        if ((coded <= tam - i + is_signed) && (j <= tam)) {
            printf("%d,%d\n", i, coded+i-is_signed);
        }
        else {
            /* senao eh uma transposicao. inicia valores de i e j */
            i = 1;
            j = 2;

            for (b = i; b < tam; b++) {
                init = tam-b;
                remove_coded = remove_coded + init;
            }

            /* encontra o valor de i */
            while(remove_coded < coded && j <= tam) {
                coded = coded - remove_coded;
                i++;
                j++;
                remove_coded = 0;
                for (b = i; b < tam; b++) {
                    init = tam-b;
                    remove_coded = remove_coded + init;
                }
            }

            /* encontra valor de j */
            init = tam-i;
            while (init < coded && j <= tam && init > 0) {
                j++;
                coded = coded - init;
                init--;
            }

            /* imprime valor de i, j e k, se existe valor de k possivel */
            if (coded+j <= tam+1)
                printf("%d,%d,%d\n", i, j, coded+j);
            /* imprime 0 caso coded seja maior que o maximo possivel */
            else
                printf("0\n");

        }
    }
    /* imprime 0 caso coded seja um numero menor que 1 */
    else
        printf("0\n");

    return 0;
}
