#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swap(int *v, int i, int j){
	int temp;

	temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}

void unrank(int n, unsigned int r, int *pi){
    // n: tamanho, r: id permutacao -1, pi: identidade
	if(n > 0){
		swap(pi,n-1,r%n);
		unrank(n-1,r/n,pi);
	}
}

void read_permutation(char strprm[], int *p) {/*{{{*/
    char *r = NULL;
    int i;

    r = strtok(strprm, ",");
    i = 0;
    while (r != NULL) {
        p[i] = atoi(r);
        i++;
        r = strtok(NULL, ",");
    }
}

void printPermutation(int *vector, int n){
	int i;
 printf("%d", vector[0]);
	for(i = 1; i < n; i++){
		printf(",%d",vector[i]);
	}
	printf("\n");
}

void createIdentity(int *vector, int n){
	int i;

	for(i = 0; i < n; i++){
		vector[i] = i+1;
	}
}

int main(int argc, char* argv[]) {
	int n, *pi2;
	unsigned int r;

    if (argc != 3) {
        printf("%s tam_perm cod_perm\n", argv[0]);
        printf("\t tam_perm  = tamanho da permutacao\n");
        printf("\t cod_perm = id da permutacao a ser decodificada\n");
        fflush(stdout);
        exit(0);
    }

	n = atoi(argv[1]); // tamanho
	r = atoi(argv[2]); // id da perm

	pi2 = malloc(sizeof(int)*n);
	createIdentity(pi2, n); // cria a permutacao identidade
	
	unrank(n, r-1, pi2); // tamanho, id permutacao - 1, identidade
	printPermutation(pi2, n);
	return 0;
}
